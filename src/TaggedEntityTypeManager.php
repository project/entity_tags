<?php

namespace Drupal\entity_tags;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Allows for entities to be tagged.
 *
 * Decorates the Entity Type Manager.
 */
class TaggedEntityTypeManager implements EntityTypeManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Create a new ErpEntityTypeManager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager Service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get entity definitions filtered by tags.
   *
   * @param array $tags
   *   The tags that this should filter on.
   * @param string $operation
   *   A string determining whether 'all' or 'any' tags should match. Defaults
   *   to 'any'.
   */
  public function getDefinitions(array $tags = [], $operation = 'any') {
    $definitions = $this->entityTypeManager->getDefinitions();

    // If no tags been specified, return all the definitions.
    if (empty($tags)) {
      return $definitions;
    }

    // Create an anonymous filter function depending on which operation is
    // specified.
    if (strtolower($operation) === 'all' && count($tags) !== 1) {
      // If the filter is 'all', make sure that every given tag is in the
      // entity's tags.
      $filter = function ($tags, $entity_tags) {
        $matches = array_intersect($tags, $entity_tags);
        return count($matches) === count($tags);
      };
    }
    elseif (strtolower($operation) === 'any' || count($tags) === 1) {
      // If the filter is 'any' or empty, or if there is only one tag, use
      // array_intersect to check whether any tags match.
      $filter = function ($tags, $entity_tags) {
        return (bool) array_intersect($tags, $entity_tags);
      };
    }
    else {
      throw new \InvalidArgumentException('The operation must be "any" or "all".');
    }

    // Check each definition. If the definition doesn't have tags, or if the
    // tags don't match the filter, remove the definition from the returned.
    foreach ($definitions as $id => $definition) {
      $additional = $definition->get('additional');
      if (!isset($additional['tags']) || !$filter($tags, $additional['tags'])) {
        unset($definitions[$id]);
      }
    }

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition($entity_type_id, $exception_on_invalid = TRUE) {
    return $this->entityTypeManager->getDefinition($entity_type_id, $exception_on_invalid);
  }

  /**
   * Gets the active definition for a content entity type.
   *
   * This is an internal method for EntityTypeManager.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   The active entity type definition.
   */
  public function getActiveDefinition($entity_type_id) {
    return $this->entityTypeManager->getDefinition($entity_type_id);
  }

  /**
   * {@inheritdoc}
   */
  public function useCaches($use_caches = FALSE) {
    return $this->entityTypeManager->useCaches($use_caches);
  }

  /**
   * {@inheritdoc}
   */
  public function getStorage($entity_type) {
    return $this->entityTypeManager->getStorage($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function hasDefinition($plugin_id) {
    return $this->entityTypeManager->hasDefinition($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteProviders($entity_type) {
    return $this->entityTypeManager->getRouteProviders($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getViewBuilder($entity_type) {
    return $this->entityTypeManager->getViewBuilder($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function hasHandler($entity_type, $handler_type) {
    return $this->entityTypeManager->hasHandler($entity_type, $handler_type);
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return $this->entityTypeManager->createInstance($plugin_id, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    return $this->entityTypeManager->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getHandler($entity_type, $handler_type) {
    return $this->entityTypeManager->getHandler($entity_type, $handler_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormObject($entity_type, $operation) {
    return $this->entityTypeManager->getFormObject($entity_type, $operation);
  }

  /**
   * {@inheritdoc}
   */
  public function getListBuilder($entity_type) {
    return $this->entityTypeManager->getListBuilder($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function createHandlerInstance($class, EntityTypeInterface $definition = NULL) {
    return $this->entityTypeManager->createHandlerInstance($class, $definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    return $this->entityTypeManager->getInstance($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessControlHandler($entity_type) {
    return $this->entityTypeManager->getAccessControlHandler($entity_type);
  }

}
