<?php

namespace Drupal\entity_tags_test\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * A test config entity type for entity tags test.
 *
 * @ContentEntityType(
 *   id = "sea",
 *   tags = {
 *     "aquatic",
 *   }
 * )
 */
class Sea extends ConfigEntityBase {

}
