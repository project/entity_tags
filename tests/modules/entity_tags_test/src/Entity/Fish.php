<?php

namespace Drupal\entity_tags_test\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * A test content entity type for entity tags test.
 *
 * @ContentEntityType(
 *   id = "fish",
 *   tags = {
 *     "animal",
 *     "aquatic",
 *   }
 * )
 */
class Fish extends ContentEntityBase {

}
