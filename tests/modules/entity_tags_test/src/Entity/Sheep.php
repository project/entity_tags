<?php

namespace Drupal\entity_tags_test\Entity;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * A test content entity type for entity tags test.
 *
 * @ContentEntityType(
 *   id = "sheep",
 *   tags = {
 *     "animal",
 *     "terrestrial",
 *   }
 * )
 */
class Sheep extends ContentEntityBase {

}
