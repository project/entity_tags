<?php

namespace Drupal\Tests\entity_tags\Kernel;

use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Tests the TaggedEntityTypeManager.
 *
 * @group entity_tags
 */
class EntityTagsTest extends KernelTestBase {

  public static $modules = ['entity_tags', 'entity_tags_test'];

  /**
   * The Tagged Entity Type Manager.
   *
   * @var \Drupal\entity_tags\TaggedEntityTypeManager
   */
  private $taggedEntityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->taggedEntityTypeManager = $this
      ->container
      ->get('entity_type.manager');
  }

  /**
   * Tests the case where no tags are passed in.
   */
  public function testDefault() {
    // The default behavior should return all entity types. Core is going to
    // provide its own entity types, so just make sure the four test types are
    // returned.
    $entity_types = $this->taggedEntityTypeManager->getDefinitions();

    $this->assertArrayHasKey('fish', $entity_types);
    $this->assertArrayHasKey('sheep', $entity_types);
    $this->assertArrayHasKey('sea', $entity_types);
    $this->assertArrayHasKey('no_tags', $entity_types);
  }

  /**
   * Tests cases where a single tag is passed in and entities match.
   */
  public function testSingleUsedTag() {
    // Test the different operations. The operation shouldn't change the result
    // with a single tag.
    foreach (['any', 'all'] as $operation) {
      $entity_types = $this
        ->taggedEntityTypeManager
        ->getDefinitions(['animal'], $operation);

      // Only fish and sheep should be in $entity_types. Do not check
      // assertEquals() as the array orders are permitted to differ.
      $this->assertArrayHasKey('fish', $entity_types);
      $this->assertArrayHasKey('sheep', $entity_types);
      $this->assertEquals(2, count($entity_types));
    }
  }

  /**
   * Tests cases where a single tag is passed in and no entities match.
   */
  public function testSingleUnusedTag() {
    // Test the different operations. The operation shouldn't change the result
    // with a single tag.
    foreach ([NULL, 'any', 'all'] as $operation) {
      $entity_types = $this
        ->taggedEntityTypeManager
        ->getDefinitions(['amphibious']);

      $this->assertEquals([], $entity_types);
    }
  }

  /**
   * Test the 'any' operator where all entities have a single matching tag.
   */
  public function testMultipleTagsAnySingleMatch() {
    // Test a combination where no entity has both.
    $entity_types = $this
      ->taggedEntityTypeManager
      ->getDefinitions(['aquatic', 'terrestrial'], 'any');

    // Each of fish, sheep, and sea should be in $entity_types. Do not check
    // assertEquals() as the array orders are permitted to differ.
    $this->assertArrayHasKey('fish', $entity_types);
    $this->assertArrayHasKey('sheep', $entity_types);
    $this->assertArrayHasKey('sea', $entity_types);
    $this->assertEquals(3, count($entity_types));
  }

  /**
   * Test the 'any' operator where entities have multiple matching tags.
   */
  public function testMultipleTagsAnyMultipleMatches() {
    // Test a combination where a single entity type has both and others also
    // have one or the other.
    $entity_types = $this
      ->taggedEntityTypeManager
      ->getDefinitions(['animal', 'aquatic'], 'any');

    // Each of fish, sheep, and sea should be in $entity_types. Do not check
    // assertEquals() as the array orders are permitted to differ.
    $this->assertArrayHasKey('fish', $entity_types);
    $this->assertArrayHasKey('sheep', $entity_types);
    $this->assertArrayHasKey('sea', $entity_types);
    $this->assertEquals(3, count($entity_types));
  }

  /**
   * Test the default operator.
   */
  public function testMultipleTagsDefaultSingleMatch() {
    // Test a combination where no entity has both.
    $entity_types = $this
      ->taggedEntityTypeManager
      ->getDefinitions(['aquatic', 'terrestrial']);

    // Each of fish, sheep, and sea should be in $entity_types. Do not check
    // assertEquals() as the array orders are permitted to differ.
    $this->assertArrayHasKey('fish', $entity_types);
    $this->assertArrayHasKey('sheep', $entity_types);
    $this->assertArrayHasKey('sea', $entity_types);
    $this->assertEquals(3, count($entity_types));
  }

  /**
   * Test the 'all' operator where an entity type has all the tags.
   */
  public function testMultipleTagsAllEntityExists() {
    // Test a combination where a single entity type has both.
    $entity_types = $this
      ->taggedEntityTypeManager
      ->getDefinitions(['animal', 'aquatic'], 'all');

    $this->assertEquals(['fish'], array_keys($entity_types));
  }

  /**
   * Test the 'all' operator where no entity type has all the tags.
   */
  public function testMultipleTagsAllEntityDoesNotExist() {
    // Test a combination where no entity type has both.
    $entity_types = $this
      ->taggedEntityTypeManager
      ->getDefinitions(['aquatic', 'terrestrial'], 'all');

    $this->assertEquals([], $entity_types);
  }

  /**
   * Test malformed operations.
   */
  public function testMalformedOperations() {
    // Test operations that should be equivalent to 'all'.
    foreach (['ALL', 'All', 'aLl'] as $operation) {
      // Use the test case from testMultipleTagsAllEntityExists().
      $entity_types = $this
        ->taggedEntityTypeManager
        ->getDefinitions(['animal', 'aquatic'], $operation);

      $this->assertEquals(['fish'], array_keys($entity_types));
    }

    // Test operations that should be equivalent to 'any'.
    foreach (['ANY', 'Any', 'aNy'] as $operation) {
      // Use the test case from testMultipleTagsAnySingleMatch().
      $entity_types = $this
        ->taggedEntityTypeManager
        ->getDefinitions(['aquatic', 'terrestrial'], $operation);

      // Each of fish, sheep, and sea should be in $entity_types. Do not check
      // assertEquals() as the array orders are permitted to differ.
      $this->assertArrayHasKey('fish', $entity_types);
      $this->assertArrayHasKey('sheep', $entity_types);
      $this->assertArrayHasKey('sea', $entity_types);
      $this->assertEquals(3, count($entity_types));
    }

    // Test operations that should throw an error.
    foreach ([' any', 'lal', 'Ayn', 3.14, ['tweezers'], 'both'] as $operation) {
      $this->expectException(\InvalidArgumentException::class);
      $this
        ->taggedEntityTypeManager
        ->getDefinitions(['aquatic', 'terrestrial'], $operation);
    }
  }

}
