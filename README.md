INTRODUCTION
------------

This module adds a service decorator to Entity Type Manager to provide the
ability to tag entity types. This module is an api tool.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit: 
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


CONFIGURATION
-------------

If Entity Tags are being implemented on a custom entity, add lines inside the
entity's annotations:
```PSR-4
 *   tags = {
 *     "tag_one",
 *     "tag_two",
 *   }
```

Otherwise, if the entity comes from a core or contrib module, add tags in
hook_entity_type_build() using:
```php
  $entity_type->set('additional', [['tags'=> ['tag_one', 'tag_two']])
```

After tags have been set on an entity type, the entity type may be selected by
tags using:
```php
\Drupal->service('entity_type.manager')->getDefinitions(['tag_one'])
```

Additionally, the operation may be set in the second argument for
getDefinitions() as "any", meaning that any entity with a listed tag will be
returned, or "all", requiring that entities have all tags to be returned.


MAINTAINERS
-----------

Current maintainers:

  * Jon Antoine https://www.drupal.org/u/jantoine
  * Charles Bamford https://www.drupal.org/u/c7bamford

This project was sponsored by:

  * ANTOINE SOLUTIONS  
    Specialized in consulting and architecting robust web applications powered
    by Drupal.
